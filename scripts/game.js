function resetGameStatus() {
  activePlayer = 0;
  currentRound = 1;
  gameIsOver = false;

  gameOverElement.querySelector('h2').innerHTML = 'You won, <span id="winner-name">PLAYER NAME</span>!';
  gameOverElement.style.display = 'none';

  let gameBoardIndex = 0;
  for (let i = 0; i < 3; i++) {
    for (let j = 0; j < 3; j++) {
      const gameBoardItemElement = gameBoardElement.children[gameBoardIndex];

      gameData[i][j] = 0;
      gameBoardItemElement.textContent = '';
      gameBoardItemElement.classList.remove('disabled');
      gameBoardIndex++;
    }
  }
}

function startNewGame() {
  if (!players[0].name || !players[1].name) {
    alert('Please set custom player names for both players!');
    return;
  }

  resetGameStatus();

  activePlayerNameElement.textContent = players[activePlayer].name;
  gameAreaElement.style.display = 'block';
}

function selectGameField(event) {
  const selectedField = event.target;

  if (selectedField.tagName !== 'LI' || gameIsOver) return;

  const selectedColumn = selectedField.dataset.col - 1;
  const selectedRow = selectedField.dataset.row - 1;

  if (gameData[selectedRow][selectedColumn] > 0) {
    alert('Please select an empty field!');
    return;
  }

  selectedField.textContent = players[activePlayer].symbol;
  selectedField.classList.add('disabled');

  gameData[selectedRow][selectedColumn] = activePlayer + 1;
  const winnerId = checkForGameOver();
  if (winnerId) endGame(winnerId);

  currentRound++;
  activePlayer = activePlayer ? 0 : 1;
  activePlayerNameElement.textContent = players[activePlayer].name;
}

function checkForGameOver() {
  // Checking the rows for equality
  for (let i = 0; i < 3; i++) {
    if (gameData[i][0] > 0 && gameData[i][0] === gameData[i][1] && gameData[i][1] === gameData[i][2]) {
      return gameData[i][0];
    }
  }

  // Checking the columns for equality
  for (let i = 0; i < 3; i++) {
    if (gameData[0][i] > 0 && gameData[0][i] === gameData[1][i] && gameData[0][i] === gameData[2][i]) {
      return gameData[0][i];
    }
  }

  //Diagonal: Top left to bottom right
  if (gameData[0][0] > 0 && gameData[0][0] === gameData[1][1] && gameData[1][1] === gameData[2][2]) {
    return gameData[0][0];
  }

  //Diagonal: Bottom left to top right
  if (gameData[2][0] > 0 && gameData[2][0] === gameData[1][1] && gameData[1][1] === gameData[0][2]) {
    return gameData[2][0];
  }

  if (currentRound === 9) return -1;

  return 0;
}

function endGame(winnerId) {
  gameIsOver = true;
  gameOverElement.style.display = 'block';

  if (winnerId > 0) {
    gameOverElement.querySelector('#winner-name').textContent = players[winnerId - 1].name;
  } else {
    gameOverElement.querySelector('h2').textContent = "It's a draw!";
  }
}
