# Tic Tac Toe



## 100 Days Of Code - 2024 Web Development Bootcamp

This is project from "[100 Days Of Code - 2024 Web Development Bootcamp](https://www.udemy.com/course/100-days-of-code-web-development-bootcamp/)" course by Academind

The project is an implementation of Tic Tac Toe game.

## Rules

1. The game is played on a grid that's 3 squares by 3 squares.
2. First player is X, second player is O . Players take turns putting their marks in empty squares.
3. The first player to get 3 of her marks in a row (up, down, across, or diagonally) is the winner.
4. When all 9 squares are full, the game is over. If no player has 3 marks in a row, the game ends in a tie.